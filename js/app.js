$(document).ready(function(){
    const toggleMenu = () => {
        $('#trigger').toggleClass('show-cross');
        $('#menu').toggleClass("show");
    }
    const togglePhone = () => {
        $('#phone').toggleClass('show-phone')
        $('#contact').toggleClass("show");
    }
    $('#trigger').on('click', () => {
        toggleMenu()
        if($(".show-phone")[0]) {
            togglePhone()
        }
    })
    $('#phone').on('click', () => {
       togglePhone()
        if($(".show-cross")[0]) {
            toggleMenu()
        }
    })
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();
        if (password !== confirmPassword) {
            $('.submit').attr('disabled', 'disabled');
            $('#CheckPasswordMatch').html("Passwords does not match!");
        }else {
            $('.submit').removeAttr('disabled');
            $("#CheckPasswordMatch").html("Passwords match.");
        }
    }
    $('#txtConfirmPassword').keyup(checkPasswordMatch);
    $(".phone_mask").mask("+38 (999) 999 99 99");
});


